export const TaskManagerActions = {
    GetTasksOverview: `/tasks/overview`,
    GetTasks: `/tasks`,
    CreateTasks: `/tasks`
}