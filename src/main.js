import "./set-public-path";
import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import routes from "./router/routes.js";
import vuetify from "@/plugins/vuetify";
import VueRouter from "vue-router";
import singleSpaVue from "single-spa-vue";
import { VueMasonryPlugin } from "vue-masonry";
import "@/assets/scss/main.scss";
import PulseLoader from "vue-spinner/src/PulseLoader.vue";

Vue.use(VueRouter);
const router = new VueRouter({
  // base: "/taskManager/",
  // mode: "history",
  routes, // short for `routes: routes`
});

// router.beforeResolve((to, from, next) => {
//   // If this isn't an initial page load.
//   if (to.name) {
//     // Start the route progress bar.
//     NProgress.start();
//   }
//   next();
// });

// router.afterEach((to, from) => {
//   // Complete the animation of the route progress bar.
//   NProgress.done();
// });

Vue.use(VueMasonryPlugin);

let v = Vue.extend({
  store,
  router,
  vuetify,
});

const vueLifecycles = singleSpaVue({
  Vue: v,
  appOptions: {
    render: (h) => h(App),
  },
});

Vue.config.productionTip = false;

export const bootstrap = vueLifecycles.bootstrap;
export const mount = vueLifecycles.mount;
export const unmount = vueLifecycles.unmount;
