// import Router from "vue-router";
// // import Overview from "../components/Overview.vue";
// // import Board from "../components/Board.vue";

// export default new Router({
//   // mode: "history",
//   // base: process.env.BASE_URL,
//   routes: [
//     {
//       path: "/",
//       name: "overview",
//       // route level code-splitting
//       // this generates a separate chunk (about.[hash].js) for this route
//       // which is lazy-loaded when the route is visited.
//       component: () =>
//         import(/* webpackChunkName: "about" */ "../components/Overview.vue"),
//     },
//     {
//       path: "/board",
//       name: "board",
//       // route level code-splitting
//       // this generates a separate chunk (about.[hash].js) for this route
//       // which is lazy-loaded when the route is visited.
//       component: () =>
//         import(/* webpackChunkName: "about" */ "../components/Board.vue"),
//     },
//   ],
// });
