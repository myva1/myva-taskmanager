import Overview from "../components/Overview.vue";
import Board from "../components/Board.vue";
import CreateTask from "../components/CreateTask.vue";
import TaskDetail from "../components/TaskDetail.vue";

const routes = [
  {
    path: "/board",
    name: "board",
    component: Board,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () =>
    //   import(/* webpackChunkName: "about" */ "../components/Board.vue"),
  },
  {
    path: "/task-detail/:taskId",
    name: "TaskDetail",
    component: TaskDetail,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () =>
    //   import(/* webpackChunkName: "about" */ "../components/Board.vue"),
  },
  {
    path: "/new-task",
    name: "NewTask",
    component: CreateTask,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () =>
    //   import(/* webpackChunkName: "about" */ "../components/Board.vue"),
  },
  {
    path: "/",
    name: "overview",
    component: Overview,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () =>
    //   import(/* webpackChunkName: "about" */ "../components/Overview.vue"),
  },
];

export default routes;
