import axios from 'axios';
import * as constants from '@/constants';

const axiosInstance = axios.create({
    baseURL: constants.TM_BASE_URL,
    timeout: 240000,
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Cache-Control": "no-cache",
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "application/json"
    }
});

axiosInstance.interceptors.request.use(function(config) {
    return config;
}, function(error) {
    return Promise.reject(error);
})

export default {
    fetch(methodName, data) {
        return axiosInstance.get(methodName, {
            params: data
        })
    },
    create(methodName, data) {
        return axiosInstance.post(methodName, data)
    },
    update(methodName, id, data) {
        return axiosInstance.put(methodName + `/${id}`, data)
    },
    delete(methodName, id) {
        return axiosInstance.delete(methodName + `/${id}`)
    },
    request(type, url, data) {
        let promise = null;
        switch(type) {
            case 'GET': promise = axios.get(url, {
                params: data
            });
                break;
            case 'POST': promise = axios.post(url, data);
                break;
            case 'PUT': promise = axios.put(url, data);
                break;
            case 'DELETE': promise = axios.delete(url, data);
                break;
            default: promise = axios.get(url, { params: data });
                break;    
        }
        return promise;
    }
}