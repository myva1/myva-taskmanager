// define app store actions names
export const ACTION_APP_INCREMENT = "ActionAppIncrement";
export const ACTION_APP_DECREMENT = "ActionAppDecrement";
export const ACTION_LOADING_START = "ActionLoadingStart";
export const ACTION_LOADING_END = "ActionLoadingEnd";
// define app store mutations names
const INCREMENT_VALUE = "IncrementValue";
const DECREMENT_VALUE = "DecrementValue";
const LOADING_STATUS = "LoadingStatus";

// initial app state
const state = {
  counter: 0,
  loadingStatus: false,
};

const getters = {
  loadingStatus(state) {
    return state.loadingStatus;
  },
  getCounter(state) {
    return state.counter;
  },
};

// app store actions
const actions = {
  [ACTION_LOADING_START](context) {
    context.commit(LOADING_STATUS, true);
  },
  [ACTION_LOADING_END](context) {
    context.commit(LOADING_STATUS, false);
  },
  [ACTION_APP_INCREMENT](context) {
    context.commit(INCREMENT_VALUE);
  },
  [ACTION_APP_DECREMENT](context) {
    context.commit(DECREMENT_VALUE);
  },
};

// app store mutations
const mutations = {
  [LOADING_STATUS](state, newLoadingStatus) {
    state.loadingStatus = newLoadingStatus;
  },
  [INCREMENT_VALUE](state) {
    state.counter = state.counter + 1;

    console.log("New counter value: " + state.counter);
  },
  [DECREMENT_VALUE](state) {
    state.counter = state.counter - 1;

    console.log("New counter value: " + state.counter);
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
};
