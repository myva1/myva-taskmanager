import Vue from "vue";
import Vuex from "vuex";
import app from "./app.store";
import overview from "./modules/overview.store";

Vue.use(Vuex);

const store = new Vuex.Store({
  debug: false,
  modules: {
    app,
    overview,
  },
});

export default store;
