import Vue from "vue";
import Vuex from "vuex";
import httprequest from "@/service/http-request.js";
import { TaskManagerActions } from '@/constants/action.js'
Vue.use(Vuex);

// initial app state
const state = {
  overviewInfo: null,
  overviewInfoStatusPending: null,
  overviewInfoStatusSuccess: null,
  overviewInfoStatusFail: null,
  taskList: []
};
const getters = {
  overviewInfo(state) {
    return state.overviewInfo;
  },
  getTaskList(state) {
    return state.taskList;
  },
};
const mutations = {
  SET_OVERVIEW_INFO(state, payload) {
    state.overviewInfo = payload;
  },
  SET_OVERVIEW_INFO_STATUS_PENDING(state, payload) {
    state.overviewInfoStatusPending = payload;
  },
  SET_OVERVIEW_INFO_STATUS_SUCCESS(state, payload) {
    state.overviewInfoStatusSuccess = payload;
  },
  SET_OVERVIEW_INFO_STATUS_FAIL(state, payload) {
    state.overviewInfoStatusFail = payload;
  },
  SET_TASK_LIST(state, payload) {
    state.taskList = payload;
  }
};
const actions = {
  getOverviewInfo(context) {
    context.commit("SET_OVERVIEW_INFO", {});
    httprequest.fetch(`${TaskManagerActions.GetTasksOverview}`)
      //success
      .then(({ data }) => {
        context.commit("SET_OVERVIEW_INFO", data);
        context.commit("SET_OVERVIEW_INFO_STATUS_SUCCESS", true);
        context.commit("SET_OVERVIEW_INFO_STATUS_FAIL", false);
      })
      //fail
      // eslint-disable-next-line no-unused-vars
      .catch((e) => {
        context.commit("SET_OVERVIEW_INFO", null);
        context.commit("SET_OVERVIEW_INFO_STATUS_SUCCESS", false);
        context.commit("SET_OVERVIEW_INFO_STATUS_FAIL", true);
      })
      //finally
      .finally(() => {
        context.commit("SET_OVERVIEW_INFO_STATUS_PENDING", false);
      });
  },
  getTaskList(context) {
    context.commit("SET_TASK_LIST", []);
    httprequest.fetch(`${TaskManagerActions.GetTasks}`)
      //success
      .then(({ data }) => {
        context.commit("SET_TASK_LIST", data);
      })
      //fail
      // eslint-disable-next-line no-unused-vars
      .catch((e) => {
        context.commit("SET_TASK_LIST", []);
      });
  },
  createTask(context, taskData) {
    httprequest.create(`${TaskManagerActions.CreateTasks}`, taskData)
      //success
      .then(({ data }) => {
        alert(data);
      })
      //fail
      // eslint-disable-next-line no-unused-vars
      .catch((e) => {
        alert(e);
      });
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
};
